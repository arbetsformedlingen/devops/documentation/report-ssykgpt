
report.pdf:
	emacs report.org --batch -f org-latex-export-to-pdf --kill



clean:
	rm -f report.pdf
